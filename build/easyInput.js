var EasyInput = {

	_track: {

		target: null,
		active: null,
		index: 0,
		hasInit: false },



	setTarget: function (element) {
		this._track.target = element instanceof jQuery ? element : $(element);
	},

	_inferType: function (element) {
		switch (element.type) {

			case 'textarea':
				return 'text';

			case 'text':
				return 'text';

			default:
				return element.type;}


	},

	_init: function () {

		if (!this._track.target)
		throw new Error("[EasyInput] No target container set");

		$(window).on('click tap', function (e) {

			if (!EasyInput._track.active)
			return;

			var wrap = $(e.target).closest('.ei-wrap');

			if (!wrap.length || wrap.get(0) !== EasyInput._track.active.get(0) || EasyInput._track.active === wrap)
			EasyInput._blur(EasyInput._track.active);

		});

	},

	_blur: function (wrap) {

		var nativeWrap = wrap instanceof jQuery ? wrap.get(0) : wrap;

		switch (this._inferType(wrap.find('[data-ei-id]').get(0))) {

			case 'select-one':
				$(wrap).find('.ei-widget').remove();
				break;}



		if (nativeWrap === this._track.active.get(0))
		this._track.active = null;

	},

	// bind EasyInput to the specified input element; applies the DOM structure, events etc.
	bind: function (element) {

		if (!this._track.hasInit)
		this._init();

		$(element).attr('data-ei-id', ++this._track.index);

		var wrap,
		hideNative = true,
		type = this._inferType(element);

		switch (type) {

			// checkbox
			case 'checkbox':
			case 'radio':
				wrap = $(element).wrap('<span class=\'ei-wrap ei-' + type + '-wrap\'></span>').closest('.ei-wrap');
				wrap.prepend('<span class=\'ei-input ei-' + type + '\' tabindex=\'0\'></span>');
				break;

			// text/textarea
			case 'text':

				wrap = $(element).wrap("<span class='ei-wrap ei-text-wrap'></span>").closest('.ei-wrap');
				wrap.prepend("<span class='ei-placeholder' aria-hidden='true'></span>");

				hideNative = false;

				break;

			// selects
			case 'select-one':

				wrap = $(element).wrap('<span class=\'ei-wrap ei-select-wrap\'></span>').closest('.ei-wrap');
				wrap.prepend('<span class=\'ei-input ei-select\' tabindex=\'0\'>' + $(element).find(':selected').text() + '</span>');

				break;

			default:
				console.warn('[EasyInput] Unrecognized input type "' + type + '"');
				break;}



		if (wrap)
		wrap.data('ei-native', element);

		if (hideNative)
		$(element).addClass('ei-hidden');

		this._bindEvents(element);

		$(element).trigger('ei-reset');

	},

	// bind events to the specified element based on it's EasyInput type (e.g. checkbox)
	_bindEvents: function (element) {

		var wrap = $(element).closest('.ei-wrap');

		switch (this._inferType(element)) {

			// checkbox
			case 'checkbox':
			case 'radio':

				$(element).on('change ei-reset', function (e, o) {

					$(this).closest('.ei-wrap').find('.ei-input').attr('data-ei-checked', this.checked);

					// if radio, reset other options (i.e. check this one, uncheck others)
					if (element.type === 'radio' && (typeof o === 'undefined' || typeof o === 'object' && o && o.preventLoop !== true))
					EasyInput._track.target.find('input[type=radio][name=\'' + element.name + '\']').not(element).trigger('ei-reset', { preventLoop: true });

				});

				wrap.find('.ei-input').on('click keypress', function (e) {

					// return/space keys can toggle state
					if (e.type === 'keypress' && !([13, 32].indexOf(e.keyCode) !== -1))
					return;

					// can't de-select radios via UI
					if ($(this).hasClass('ei-radio') && $(this).attr('data-ei-checked') === 'true')
					return;

					$(element).prop('checked', !element.checked).trigger('change');

					if (e.type === 'click')
					$(this).blur();

				});

				break;

			// text/textarea
			case 'text':
				$(element).on('focus blur keydown keyup ei-reset', function (e) {

					var holder = wrap.find('.ei-placeholder');

					// reset html to value of placeholder attr, in case it's updated since init
					if (e.type === 'focus' || e.type === 'ei-reset')
					holder.html(wrap.find('input, textarea').attr('placeholder'));

					wrap.find('.ei-placeholder').toggleClass('ei-active', this.value.length > 0);

				});
				break;

			case 'select-one':

				wrap.find('.ei-input').on('click ei-reset', function () {

					wrap.find('.ei-widget').remove();

					var widget = document.createElement('aside');
					widget.className = 'ei-widget ei-options';

					var optionHtml = '';
					for (var n of element.getElementsByTagName('option')) {
						optionHtml += '<div class=\'ei-option\' data-value=\'' + n.value + '\' ' + (n.disabled ? 'data-disabled' : 'tabindex=\'0\'') + '>' + n.innerText + '</div>';}

					widget.innerHTML = optionHtml;
					wrap.get(0).appendChild(widget);

					EasyInput._track.active = wrap;

				});

				wrap.on('click', '.ei-option', function () {

					$(wrap.data('ei-native')).val($(this).attr('data-value'));

					wrap.find('.ei-input').html($(this).text());

					EasyInput._blur(wrap);

				});

				break;}



	},

	// find all elements with a class of 'ei' and bind/init
	bindAll: function () {

		this._track.target.find('.ei:not([data-ei-id])').each(function () {
			EasyInput.bind(this);
		});

	} };



var ei = EasyInput;

$(function () {

	ei.setTarget($('body'));

	ei.bindAll();

});
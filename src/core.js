﻿const EasyInput = {

	_track: {

		index: 0,
		target: null,
		active: null,
		handlers: [],
		hasInit: false

	},

	_init() {

		if (!this._track.target)
			throw new Error("[EasyInput] No target container set");

		this._track.target.on('click tap', function (e) {

			if (!EasyInput._track.active)
				return;

			const wrap = $(e.target).closest('.ei-wrap');

			if (!wrap.length || wrap.get(0) !== EasyInput._track.active.get(0) || EasyInput._track.active === wrap)
				EasyInput._blur(EasyInput._track.active);

		});

		this._track.target.on('ei-reset', '.ei-wrap', function () {
			
			const element = $($(this).data('ei-native'));

			if (element.length && typeof element.data('ei-handler').reset === 'function')
				element.data('ei-handler').reset.call(this, element.get(0), $(this).get(0));

		});

	},

	setTarget(element) {
		this._track.target = element instanceof jQuery ? element : $(element);
	},

	registerHandler(handler) {
		this._track.handlers.push(handler);
	},

	_dispatch(event, element) {
		$(element).trigger(event);
	},

	_focus(wrap) {

		this._track.active = wrap;

		$(wrap).addClass('ei-active').find('.ei-input').addClass('ei-active');

	},

	_blur(wrap) {

		const nativeWrap = wrap instanceof jQuery ? wrap.get(0) : wrap;

		if (nativeWrap === this._track.active.get(0))
			this._track.active = null;

		$(wrap).removeClass('ei-active').find('.ei-widget').hide().end().find('.ei-input').removeClass('ei-active');

		this._dispatch('blur', $(wrap).data('ei-native'));

	},

	bind(element) {

		if ($(element).attr('data-ei-id'))
			return;

		if (!this._track.hasInit)
			this._init();

		$(element).attr('data-ei-id', ++this._track.index);

		const handlers = this._track.handlers.filter(function (n) {
			return $(element).is(n.target);
		});

		if (!handlers.length) {
			console.error("[EasyInput] No suitable handler found", element);
			return;
		}

		const handler = handlers[0];

		$(element).data('ei-handler', handler);

		const wrap = $(element).wrap(`<span class='ei-wrap ei-${handler.key}-wrap'></span>`).closest('.ei-wrap');

		wrap.data('ei-native', $(element).get());

		if (handler.masksNativeElement === true)
			$(element).addClass('ei-hidden');

		handler.mask.call(handler, element, wrap, this._track.target);
		handler.bind.call(handler, element, wrap, this._track.target);

	},

	bindAll() {
		this._track.target.find('.ei:not([data-ei-id])').each(function () {
			EasyInput.bind(this);
		});
	}

};

$(function () {
	EasyInput.setTarget($('body'));
	EasyInput.bindAll();
});
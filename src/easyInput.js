const EasyInput = {

	_track: {

		index: 0,
		target: null,
		active: null,
		handlers: [],
		hasInit: false

	},

	_init() {

		if (!this._track.target)
			throw new Error("[EasyInput] No target container set");

		this._track.target.on('click tap', function (e) {

			if (!EasyInput._track.active)
				return;

			const wrap = $(e.target).closest('.ei-wrap');

			if (!wrap.length || wrap.get(0) !== EasyInput._track.active.get(0) || EasyInput._track.active === wrap)
				EasyInput._blur(EasyInput._track.active);

		});

		this._track.target.on('ei-reset', '.ei-wrap', function () {
			
			const element = $($(this).data('ei-native'));

			if (element.length && typeof element.data('ei-handler').reset === 'function')
				element.data('ei-handler').reset.call(this, element.get(0), $(this).get(0));

		});

	},

	setTarget(element) {
		this._track.target = element instanceof jQuery ? element : $(element);
	},

	registerHandler(handler) {
		this._track.handlers.push(handler);
	},

	_dispatch(event, element) {
		$(element).trigger(event);
	},

	_focus(wrap) {

		this._track.active = wrap;

		$(wrap).addClass('ei-active').find('.ei-input').addClass('ei-active');

	},

	_blur(wrap) {

		const nativeWrap = wrap instanceof jQuery ? wrap.get(0) : wrap;

		if (nativeWrap === this._track.active.get(0))
			this._track.active = null;

		$(wrap).removeClass('ei-active').find('.ei-widget').hide().end().find('.ei-input').removeClass('ei-active');

		this._dispatch('blur', $(wrap).data('ei-native'));

	},

	bind(element) {

		if ($(element).attr('data-ei-id'))
			return;

		if (!this._track.hasInit)
			this._init();

		$(element).attr('data-ei-id', ++this._track.index);

		const handlers = this._track.handlers.filter(function (n) {
			return $(element).is(n.target);
		});

		if (!handlers.length) {
			console.error("[EasyInput] No suitable handler found", element);
			return;
		}

		const handler = handlers[0];

		$(element).data('ei-handler', handler);

		const wrap = $(element).wrap(`<span class='ei-wrap ei-${handler.key}-wrap'></span>`).closest('.ei-wrap');

		wrap.data('ei-native', $(element).get());

		if (handler.masksNativeElement === true)
			$(element).addClass('ei-hidden');

		handler.mask.call(handler, element, wrap, this._track.target);
		handler.bind.call(handler, element, wrap, this._track.target);

	},

	bindAll() {
		this._track.target.find('.ei:not([data-ei-id])').each(function () {
			EasyInput.bind(this);
		});
	}

};

$(function () {
	EasyInput.setTarget($('body'));
	EasyInput.bindAll();
});
EasyInput.registerHandler({

	key: 'checkbox',

	target: 'input[type=checkbox]',

	masksNativeElement: true,

	mask: function (element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-checkbox' tabindex='0'></span>`);
	},

	bind: function (element, wrap, scope) {

		$(element).on('change ei-reset', function (e, o) {
			$(wrap).find('.ei-input').attr('data-ei-checked', element.checked);
		});

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return/space keys can toggle state
			if (e.type === 'keypress' && ![13, 32].includes(e.keyCode))
				return;

			$(element).prop('checked', !element.checked).trigger('change');

			if (e.type === 'click')
				$(this).blur();

		});

	}

});
EasyInput.registerHandler({

	key: 'radio',

	target: 'input[type=radio]',

	masksNativeElement: true,

	mask: function (element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-radio' tabindex='0'></span>`);
	},

	bind: function (element, wrap, scope) {

		$(element).on('change ei-reset', function (e, o) {

			$(wrap).find('.ei-input').attr('data-ei-checked', this.checked);

			// reset other options (i.e. check this one, uncheck others)
			if (typeof o === 'undefined' || (typeof o === 'object' && o && o.preventLoop !== true))
				$(scope).find(`input[type=radio][name='${element.name}']`).not(element).trigger('ei-reset', { preventLoop: true });

		});

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return/space keys can toggle state
			if (e.type === 'keypress' && ![13, 32].includes(e.keyCode))
				return;

			// can't de-select radios via UI
			if (element.checked)
				return;

			$(element).prop('checked', !element.checked).trigger('change');

			if (e.type === 'click')
				$(this).blur();

		});

	}

});
EasyInput.registerHandler({

	key: 'select',

	target: 'select',

	masksNativeElement: true,

	mask(element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-select' tabindex='0'>${$(element).find(':selected').text()}</span>`);
	},

	reset(element, wrap) {

		$(wrap).find('.ei-widget').remove();

		const widget = $("<aside class='ei-widget ei-options' style='display: none;'></aside>");

		var optionHtml = '';
		for (let n of $(element).find('option').get())
			optionHtml += `<div class='ei-option' data-value='${n.value}' ${n.disabled ? `data-disabled` : `tabindex='0'`}>${n.innerText}</div>`;

		widget.html(optionHtml).appendTo($(wrap));

	},

	bind(element, wrap, scope) {

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return key can toggle state
			if (e.type === 'keypress' && ![13].includes(e.keyCode))
				return;

			if (EasyInput._track.active === wrap) {
				EasyInput._blur(wrap);
				return;
			}

			EasyInput._focus(wrap);

			$(element).trigger('ei-reset');

			$(wrap).find('.ei-options').toggle();

		});

		$(wrap).on('click', '.ei-option', function () {

			$(wrap.data('ei-native')).val($(this).attr('data-value'));

			wrap.find('.ei-input').html($(this).text());

			EasyInput._blur(wrap);

		});

	}

});
EasyInput.registerHandler({

	key: 'text',

	target: 'input[type=text], textarea',

	masksNativeElement: false,

	mask: function (element, wrap, scope) {

		$(wrap).prepend("<span class='ei-placeholder' aria-hidden='true'></span>");

		$(element).addClass('ei-input');

	},

	reset(element, wrap) {

		const holder = $(wrap).find('.ei-placeholder');

		// reset html to value of placeholder attr, in case it's updated since init
		holder.html(element.placeholder);

		holder.toggleClass('ei-active', element.value.length > 0);

	},

	bind: function (element, wrap, scope) {

		$(element).on('focus blur', function () {
			$(element).trigger('ei-reset');
		});

		$(element).on('keydown keyup', function () {
			$(wrap).find('.ei-placeholder').toggleClass('ei-active', element.value.length > 0);
		});

		this.reset(element, wrap);

	}

});
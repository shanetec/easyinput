﻿EasyInput.registerHandler({

	key: 'select',

	target: 'select',

	masksNativeElement: true,

	mask(element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-select' tabindex='0'>${$(element).find(':selected').text()}</span>`);
	},

	reset(element, wrap) {

		$(wrap).find('.ei-widget').remove();

		const widget = $("<aside class='ei-widget ei-options' style='display: none;'></aside>");

		var optionHtml = '';
		for (let n of $(element).find('option').get())
			optionHtml += `<div class='ei-option' data-value='${n.value}' ${n.disabled ? `data-disabled` : `tabindex='0'`}>${n.innerText}</div>`;

		widget.html(optionHtml).appendTo($(wrap));

	},

	bind(element, wrap, scope) {

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return key can toggle state
			if (e.type === 'keypress' && ![13].includes(e.keyCode))
				return;

			if (EasyInput._track.active === wrap) {
				EasyInput._blur(wrap);
				return;
			}

			EasyInput._focus(wrap);

			$(element).trigger('ei-reset');

			$(wrap).find('.ei-options').toggle();

		});

		$(wrap).on('click', '.ei-option', function (k) {
			if (typeof k._EasyInputSelfInit === 'undefined')
				$(wrap.data('ei-native')).val($(this).attr('data-value')).trigger('change', { _EasyInputSelfInit: true });
		});

		$(element).on('change', function (k) {

			if (typeof k._EasyInputSelfInit === 'undefined')
				return;

			wrap.find('.ei-input').html($(this).text());

			EasyInput._blur(wrap);

		});

	}

});
﻿EasyInput.registerHandler({

	key: 'text',

	target: 'input[type=text], textarea',

	masksNativeElement: false,

	mask: function (element, wrap, scope) {

		$(wrap).prepend("<span class='ei-placeholder' aria-hidden='true'></span>");

		$(element).addClass('ei-input');

	},

	reset(element, wrap) {

		const holder = $(wrap).find('.ei-placeholder');

		// reset html to value of placeholder attr, in case it's updated since init
		holder.html(element.placeholder);

		holder.toggleClass('ei-active', element.value.length > 0);

	},

	bind: function (element, wrap, scope) {

		$(element).on('focus blur', function () {
			$(element).trigger('ei-reset');
		});

		$(element).on('keydown keyup', function () {
			$(wrap).find('.ei-placeholder').toggleClass('ei-active', element.value.length > 0);
		});

		this.reset(element, wrap);

	}

});
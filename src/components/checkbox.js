﻿EasyInput.registerHandler({

	key: 'checkbox',

	target: 'input[type=checkbox]',

	masksNativeElement: true,

	mask: function (element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-checkbox' tabindex='0'></span>`);
	},

	bind: function (element, wrap, scope) {

		$(element).on('change ei-reset', function (e, o) {
			$(wrap).find('.ei-input').attr('data-ei-checked', element.checked);
		});

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return/space keys can toggle state
			if (e.type === 'keypress' && ![13, 32].includes(e.keyCode))
				return;

			$(element).prop('checked', !element.checked).trigger('change');

			if (e.type === 'click')
				$(this).blur();

		});

	}

});
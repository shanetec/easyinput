﻿EasyInput.registerHandler({

	key: 'radio',

	target: 'input[type=radio]',

	masksNativeElement: true,

	mask: function (element, wrap, scope) {
		$(wrap).prepend(`<span class='ei-input ei-radio' tabindex='0'></span>`);
	},

	bind: function (element, wrap, scope) {

		$(element).on('change ei-reset', function (e, o) {

			$(wrap).find('.ei-input').attr('data-ei-checked', this.checked);

			// reset other options (i.e. check this one, uncheck others)
			if (typeof o === 'undefined' || (typeof o === 'object' && o && o.preventLoop !== true))
				$(scope).find(`input[type=radio][name='${element.name}']`).not(element).trigger('ei-reset', { preventLoop: true });

		});

		$(wrap).find('.ei-input').on('click keypress', function (e) {

			// return/space keys can toggle state
			if (e.type === 'keypress' && ![13, 32].includes(e.keyCode))
				return;

			// can't de-select radios via UI
			if (element.checked)
				return;

			$(element).prop('checked', !element.checked).trigger('change');

			if (e.type === 'click')
				$(this).blur();

		});

	}

});